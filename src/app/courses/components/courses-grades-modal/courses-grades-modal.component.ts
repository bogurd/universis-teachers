import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import 'smartwizard';
import { FormBuilder} from '@angular/forms';
import {CoursesService} from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';
import {ErrorService} from '@universis/common';
import {Subject} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-courses-grades-modal',
  templateUrl: './courses-grades-modal.component.html'
})
export class CoursesGradesModalComponent implements OnInit {

  public courseExamId: number;      // current courseExam ID
  public resultsOfUpload;           // results of uploading file
  public resultsOfSubmission;       // results of complete action
  public courseExam;                // current courseExam object
  public waitForResults = false;    // This flag help go to next step if validate is success
  public errorMessage = '';
  public message = '';
  public haveChangesGrades = false;
  public counters = [];             // counters with sums for every grade result state

  @ViewChild('wizard') wizard: ElementRef;
  public selectedFile: File;        // current selected file for upload

  public result: Subject<boolean> = new Subject<boolean>(); // result for modal closing
  public resultSuccessStatus = false;      // true : reload data

  constructor(private modalRef: BsModalRef,
              private fb: FormBuilder,
              private courseServices: CoursesService,
              private translate: TranslateService,
              private loadingService: LoadingService,
              private  errorService: ErrorService) {
  }

  ngOnInit() {

    this.getCourseExam(this.courseExamId);
    const self = this;

    $(this.wizard.nativeElement).smartWizard({
      anchorSettings: {
        anchorClickable: true,           // Enable/Disable anchor navigation
        enableAllAnchors: false,         // Activates all anchors clickable all times
        markDoneStep: true,              // add done css
        enableAnchorOnDoneStep: true,    // Enable/Disable the done steps navigation
      },
      useURLhash: false,
      showStepURLhash: false,
      lang: {
        next: this.translate.instant('CoursesLocal.Modal.Step1.Next'),
        previous: this.translate.instant('CoursesLocal.Modal.Buttons.Prev')
      },
      toolbarSettings: {
        toolbarExtraButtons: [
          $('<button></button>').text(this.translate.instant('CoursesLocal.Modal.Buttons.Close'))
            .addClass('btn btn-secondary btn-close')
            .on('click', function () {
              self.closeModal();
            })
        ]
      },
    });

    // call next function and validate data
    $(this.wizard.nativeElement).on('leaveStep', function (e, anchorObject, stepNumber, stepDirection) {
      if (!self.selectedFile) {
        self.errorMessage = self.translate.instant('CoursesLocal.Modal.Step1.NoFoundFile');
        return false;
      }

      if (stepDirection === 'forward' && stepNumber === 0) { // current 1st step go to 2nd step
        if (!self.waitForResults) {
          self.loadingService.showLoading();
          self.uploadGradesFile();
          return false;
        }
      } else if (stepDirection === 'forward' && stepNumber === 1) { // current 2nd step go to 3rd step
        // initialize message values
        self.message = '';
        self.errorMessage = '';

        if (self.haveChangesGrades) {
          self.loadingService.showLoading();
          self.setUploadToComplete();
        } else {

          return false;
        }
      }
    });

    // runs at the loading step
    $(this.wizard.nativeElement).on('showStep', function (e, anchorObject, stepNumber, stepDirection) {

      if (stepNumber === 0 && stepDirection === 'backward') { // 1st step
        self.counters = [];
        self.waitForResults = false;
        self.message = '';
        self.errorMessage = '';
        self.haveChangesGrades = false;
        self.hideButtonPrevious(true);
      }

      if (stepNumber === 0) { // 1st step

        self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step1.Next'));
        self.hideButtonPrevious(true);

      } else if (stepNumber === 1) { // 2nd step

        self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step2.Next'));
        self.hideButtonNext(false);
        self.hideButtonPrevious(false);

        if (self.counters.some(c => c.code === 'INS') || self.counters.some(c => c.code === 'UPD')) {
          self.disabledButtonNext(false);
        } else {
          self.disabledButtonNext(true);
        }

      } else if (stepNumber === 2) { // 3rd step

        self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step3.Next'));
        self.hideButtonNext(true);
        self.hideButtonPrevious(false);
      }
    });
  }

  // second step
  uploadGradesFile() {
    if (this.selectedFile) {
      this.courseServices.uploadGradesFile(this.selectedFile, this.courseExamId).subscribe(res => {
        this.resultsOfUpload = res;

        if (this.resultsOfUpload.object.validationResult.success) {
          this.errorMessage = '';
          this.message = this.resultsOfUpload.object.validationResult.message;

          // group students by validation result code
          for (const grade of this.resultsOfUpload.object.grades) {
            if (grade.validationResult.code !== 'UNMOD') {
              this.haveChangesGrades = true;
              let index = -1;

              if (this.counters.length > 0) {
                index = this.counters.findIndex(c => c.code === grade.validationResult.code);
              }

              if (index >= 0) {
                this.counters[index].counter++;
              } else {
                this.counters.push(new Counter(
                  grade.validationResult.code,
                  1,
                  grade.validationResult.message
                ));
              }
            }
          }
          this.waitForResults = true;
          $(this.wizard.nativeElement).smartWizard('next'); // go to second step
          this.loadingService.hideLoading();
        } else {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.NotValidFile');
          this.loadingService.hideLoading();
        }
      }, (err) => {
        // whne statusCode of validationResult exist get this

        const statusCode = err.error && err.error.object && err.error.object.validationResult ? err.error.object.validationResult.statusCode : 0;

        // get error message key
        const errorMessageKey = `CoursesLocal.Modal.Errors.E${statusCode || err.status || 500}.message`;
        // try to get localized message
        const errorMessage = this.translate.instant(errorMessageKey);
        // if custom error does not exist errorMessage should be equal to errorMessageKey
        if (errorMessage === errorMessageKey) {
          // get error message from common http errors
          this.errorMessage = this.translate.instant(`E${statusCode || err.status }.message`);
        } else {
          // set custom http error message
          this.errorMessage = errorMessage;
        }
        this.loadingService.hideLoading();
      });
    }
  }

  // thirth step
  setUploadToComplete() {
    if (this.selectedFile) {
      this.courseServices.setUploadToComplete(this.courseExamId, this.resultsOfUpload.id).then(res => {
        this.resultsOfSubmission = res;
        this.message = this.resultsOfSubmission.object.validationResult.message;

        if (this.resultsOfSubmission.object.validationResult.success) {
          if (this.resultsOfSubmission.object.validationResult.code === 'PSUCC') {
            this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.PartialSuccess');
          }
          this.hideButtonPrevious(true);
        } else {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.NotSuccessSubmit');
          this.hideButtonPrevious(false);
        }
        this.resultSuccessStatus = true;
        this.loadingService.hideLoading();
      }, (err) => {
        // get error message key
        const errorMessageKey = `CoursesLocal.Modal.Errors.E${err.status}.message`;
        // try to get localized message
        const errorMessage = this.translate.instant(errorMessageKey);
        // if custom error does not exist errorMessage should be equal to errorMessageKey
        if (errorMessage === errorMessageKey) {
          // get error message from common http errors
          this.errorMessage = this.translate.instant(`E${err.status}.message`);
        } else {
          // set custom http error message
          this.errorMessage = errorMessage;
        }
        this.loadingService.hideLoading();
      });
    }
  }

  getCourseExam(courseExamId) {
    this.courseServices.getCurrentCourseExams(this.courseExamId).then(res => {
      this.courseExam = res;
      if (this.courseExam.status.alternateName === 'closed') {
        this.disabledButtonNext(true);
      } else {
        this.disabledButtonNext(false);
      }
    }).catch(err => {
      return this.errorService.navigateToError(err);
    });
  }

  getInputFileName(newFile) {
    this.selectedFile = newFile;
    this.disabledButtonNext(false);
    this.errorMessage = '';
  }


  closeModal() {
    this.result.next(this.resultSuccessStatus);
    this.modalRef.hide();
  }

  disabledButtonNext(status) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    if (status) {
      btnnext.setAttribute('disabled', 'true');
    } else {
      btnnext.removeAttribute('disabled');
    }
  }

  hideButtonNext(status) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    if (status) {
      btnnext.setAttribute('style', 'display:none');
    } else {
      btnnext.setAttribute('style', 'display:block');
    }
  }

  hideButtonPrevious(status) {
    const btnprev = document.getElementsByClassName('sw-btn-prev')[0];
    if (status) {
      btnprev.setAttribute('style', 'display:none');
    } else {
      btnprev.setAttribute('style', 'display:block');
    }
  }

  changeTextOnButtonNext(text) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    btnnext.innerHTML = text;
  }


}

export class Counter {

  constructor(
    public code = '',
    public counter = 0,
    public message = ''
  ) {}
}
